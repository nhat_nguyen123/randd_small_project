import 'package:flutter_getx_template/third_party_apis/google_ml_text_recognition/google_ml_text_recognition.dart';

class JapanDrivingLicense {
  final String lastAndFirstName;
  final String dateOfBirth;
  final String address;
  final String dateIssueOfCard;
  final int issueCardNumber;
  final String dateExpireOfCard;
  final String conditions;
  final String backgroundLevel;
  final String licenseNumber;
  final String dateOfFirstIssueOfMotocycleLicenses;
  final String dateOfFirstIssueOfOtherLicenses;
  final String dateOfFirstIssueOfCommercialLicenses;
  final List<String> validCategories;
  final String issuingAuthority;

  JapanDrivingLicense({
    required this.lastAndFirstName,
    required this.dateOfBirth,
    required this.address,
    required this.dateIssueOfCard,
    required this.issueCardNumber,
    required this.dateExpireOfCard,
    required this.conditions,
    required this.backgroundLevel,
    required this.licenseNumber,
    required this.dateOfFirstIssueOfCommercialLicenses,
    required this.dateOfFirstIssueOfOtherLicenses,
    required this.dateOfFirstIssueOfMotocycleLicenses,
    required this.validCategories,
    required this.issuingAuthority,
  });

  factory JapanDrivingLicense.parseFromLinesOfText(List<LineInfo> lines) {
    String dateOfBirth = "";
    for (final line in lines) {
      /*
      final RegExp pattern = RegExp(r".*(昭和36\s*年\s*4\s*月27\s*日生).*");
      final RegExpMatch? match = pattern.firstMatch(line.text);
      dateOfBirth = match?.group(0).toString() ?? "";
      print('DateOfBirth: ${match.toString()}');
      if (line.text == "昭和36 年 4月27 日生)") print("can compare japanese.");
      */

    }

    return JapanDrivingLicense(
      lastAndFirstName: "",
      dateOfBirth: dateOfBirth,
      address: "",
      dateIssueOfCard: "",
      issueCardNumber: 100,
      dateExpireOfCard: "",
      conditions: "",
      backgroundLevel: "",
      licenseNumber: "",
      dateOfFirstIssueOfCommercialLicenses: "",
      dateOfFirstIssueOfOtherLicenses: "",
      dateOfFirstIssueOfMotocycleLicenses: "",
      validCategories: [
        "",
      ],
      issuingAuthority: "",
    );
  }
}

enum JapanDrivingLicenseInfo {
  lastAndFirstName,
  dateOfBirth,
  address,
  dateIssueOfCard,
  issueCardNumber,
  dateExpireOfCard,
  conditions,
  backgroundLevel,
  licenseNumber,
  dateOfFirstIssueOfCommercialLicenses,
  dateOfFirstIssueOfOtherLicenses,
  dateOfFirstIssueOfMotocycleLicenses,
  validCategories,
  issuingAuthority,
}

abstract class JapanDrivingLicenseRegExp {
  static Map<JapanDrivingLicenseInfo, RegExp> patterns = {
    JapanDrivingLicenseInfo.lastAndFirstName: RegExp(""),
    JapanDrivingLicenseInfo.dateOfBirth:
        RegExp(r".*(昭和\d{2}\s*年\s*\d{1}\s*月\d{2}\s*日生).*"),
    JapanDrivingLicenseInfo.address: RegExp(""),
    JapanDrivingLicenseInfo.dateIssueOfCard:
        RegExp(r"(平成\d{2}\s*年\s*\d{2}\s*月\s*\d{2}\s*日)\s*\d{4}"),
    JapanDrivingLicenseInfo.issueCardNumber:
        RegExp(r"(平成\d{2}\s*年\s*\d{2}\s*月\s*\d{2}\s*日)\s*(\d{4})"),
    JapanDrivingLicenseInfo.dateExpireOfCard:
        RegExp(r"(平成\d{2}年\d{2}月\d{2}日\s*まで有効)[一-龯]*"),
    JapanDrivingLicenseInfo.conditions: RegExp("([一-龯]{3})"),
    JapanDrivingLicenseInfo.backgroundLevel: RegExp("[一-龯]+"),
    JapanDrivingLicenseInfo.licenseNumber: RegExp(r"\d{6,}"),
    JapanDrivingLicenseInfo.dateOfFirstIssueOfCommercialLicenses:
        RegExp(r"(平版\d{2}年\d{2}月\d{2}日)"),
    JapanDrivingLicenseInfo.dateOfFirstIssueOfOtherLicenses:
        RegExp(r"(平版\d{2}年\d{2}月\d{2}日)"),
    JapanDrivingLicenseInfo.dateOfFirstIssueOfMotocycleLicenses:
        RegExp(r"(平版\d{2}年\d{2}月\d{2}日)"),
    JapanDrivingLicenseInfo.validCategories: RegExp(""),
    JapanDrivingLicenseInfo.issuingAuthority: RegExp("[一-龯]+"),
  };
}
