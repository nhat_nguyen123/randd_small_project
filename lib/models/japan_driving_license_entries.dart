abstract class JapanDrivingLicenseEntries {
  final String lastAndFirstName = "氏名";
  final String address = "住所";
  final String dateIssueOfCard = "交付";
  final String conditionsFirstPart = "免許の";
  final String conditionSecondPart = "条件等";
  final String licenseNumber = "番号";
  final String dateOfFirstIssueOfMotocycleLicenses = "二•小•原	";
  final String dateOfFirstIssueOfOtherLicenses = "他";
  final String dateOfFirstIssueOfCommercialLicenses = "二種";
  final String validCategoriesFirstPart = "種類";
  final String validCategoriesSecondPart = "類";
  final String issuingAuthority = "公安委員会";
}
