part of 'app_pages.dart';

abstract class AppRoutes {
  static const splash = '/';
  static const productList = '/product-list';
  static const cart = '/cart';
  static const licenseScan = '/license-scan';
}
