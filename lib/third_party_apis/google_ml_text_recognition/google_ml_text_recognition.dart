import 'package:google_ml_kit/google_ml_kit.dart';

class GoogleMLTextRecognition {
  static Future<List<LineInfo>> processDetectingText({
    required String imagePath,
  }) async {
    try {
      final inputImage = InputImage.fromFilePath(imagePath);
      final textDetector = GoogleMlKit.vision.textDetectorV2();
      final RecognisedText recognisedText = await textDetector.processImage(
        inputImage,
        script: TextRecognitionOptions.JAPANESE,
      );
      final List<TextBlock> blocks = recognisedText.blocks;
      return getRecognizedLines(blocks);
    } catch (e) {
      print('Error when proccessing image with googleMLkit: $e');
      return List.empty();
    }
  }

  static List<LineInfo> getRecognizedLines(List<TextBlock> blocks) {
    final List<LineInfo> recognizedLines = List.empty(growable: true);
    for (final block in blocks) {
      for (final line in block.lines) {
        final List<Point> points = List.empty(growable: true);
        for (final offset in line.cornerPoints) {
          points.add(
            Point(x: offset.dx, y: offset.dy),
          );
        }
        recognizedLines.add(
          LineInfo(
            coordinates: points,
            text: line.text,
          ),
        );
      }
    }
    return recognizedLines;
  }
}

class LineInfo {
  List<Point> coordinates;
  final String text;

  LineInfo({
    required this.coordinates,
    required this.text,
  });
}

class Point {
  final double x;
  final double y;

  Point({
    required this.x,
    required this.y,
  });
}
