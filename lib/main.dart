import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_getx_template/models/japan_driving_license.dart';
import 'package:flutter_getx_template/third_party_apis/google_ml_text_recognition/google_ml_text_recognition.dart';
import 'package:flutter_getx_template/third_party_apis/google_ml_text_recognition/google_ml_text_recognition.dart';
import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:image/image.dart' as ImageLib;
import 'package:path_provider/path_provider.dart';

Future<void> mainDelegate() async {
  // Ensure that plugin services are initialized so that `availableCameras()`
  // can be called before `runApp()`
  WidgetsFlutterBinding.ensureInitialized();

  // Obtain a list of the available cameras on the device.
  final cameras = await availableCameras();

  // Get a specific camera from the list of available cameras.
  final firstCamera = cameras.first;

  runApp(
    MaterialApp(
      theme: ThemeData.dark(),
      home: TakePicturePage(
        // Pass the appropriate camera to the TakePicturePage widget.
        camera: firstCamera,
      ),
    ),
  );
}

// A screen that allows users to take a picture using a given camera.
class TakePicturePage extends StatefulWidget {
  const TakePicturePage({
    Key? key,
    required this.camera,
  }) : super(key: key);

  final CameraDescription camera;

  @override
  TakePicturePageState createState() => TakePicturePageState();
}

class TakePicturePageState extends State<TakePicturePage> {
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    // To display the current output from the Camera,
    // create a CameraController.
    _controller = CameraController(
      // Get a specific camera from the list of available cameras.
      widget.camera,
      // Define the resolution to use.
      ResolutionPreset.medium,
    );

    // Next, initialize the controller. This returns a Future.
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // Dispose of the controller when the widget is disposed.
    _controller.dispose();
    super.dispose();
  }

  Future<void> _processImage() async {
    try {
      // Ensure that the camera is initialized.
      await _initializeControllerFuture;

      // final xFileImage = await _controller.takePicture();
      final ByteData imageData = await NetworkAssetBundle(
        Uri.parse(
          "https://i.pinimg.com/originals/47/66/8d/47668d007a5fa1bc78b33c51d63d3724.jpg",
        ),
      ).load("");
      final Uint8List bytes = imageData.buffer.asUint8List();
      final directory = await getApplicationDocumentsDirectory();
      final imageFile =
          await File('${directory.path}/license.png').writeAsBytes(bytes);

      setState(
        () {
          _isLoading = true;
        },
      );
      final List<LineInfo> recognizedLines =
          await GoogleMLTextRecognition.processDetectingText(
        imagePath: imageFile.path,
      );
      final JapanDrivingLicense license =
          JapanDrivingLicense.parseFromLinesOfText(recognizedLines);
      setState(
        () {
          _isLoading = false;
        },
      );

      await Navigator.of(context).push(
        MaterialPageRoute(
          builder: (context) => DisplayTextPage(
            imagePath: imageFile.path,
          ),
        ),
      );
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Take a picture')),
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (_isLoading) {
            return const Center(child: CircularProgressIndicator());
          }
          if (snapshot.connectionState == ConnectionState.done) {
            return CameraPreview(_controller);
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _processImage,
        child: const Icon(Icons.camera_alt),
      ),
    );
  }
}

class DisplayTextPage extends StatefulWidget {
  final String imagePath;

  const DisplayTextPage({
    Key? key,
    required this.imagePath,
  }) : super(key: key);

  @override
  State<DisplayTextPage> createState() => _DisplayTextPageState();
}

class _DisplayTextPageState extends State<DisplayTextPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Display output')),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(16),
            child: Image.file(
              File(widget.imagePath),
              width: MediaQuery.of(context).size.width,
              height: 300,
            ),
          ),
        ],
      ),
    );
  }
}
